from django.shortcuts import render
from django.views.generic import TemplateView

from ProjectMySite.models import Dish


class HomeView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = {
            'dishes': Dish.objects.all()
        }
        return context


class ProjectView(TemplateView):
    template_name = "base.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['afasf', 'aefaf', 'aefaef']

        }
        return context


class Page1View(TemplateView):
    template_name = "page1.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['page1','afasf', 'aefaf', 'aefaef']

        }
        return context


class Page2View(TemplateView):
    template_name = "page2.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['page2','afasf', 'aefaf', 'aefaef']

        }
        return context


class Page3View(TemplateView):
    template_name = "page3.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['page3','afasf', 'aefaf', 'aefaef']

        }
        return context
