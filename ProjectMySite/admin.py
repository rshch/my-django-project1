from django.contrib import admin

from ProjectMySite.models import Dish

admin.site.register(Dish)