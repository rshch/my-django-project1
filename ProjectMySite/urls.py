from django.conf.urls import include, url
from django.contrib import admin

from ProjectMySite.views import ProjectView, Page3View, Page2View, Page1View, HomeView

urlpatterns = [
    url(r'^$', HomeView.as_view()),
    url(r'^admin/', admin.site.urls),
    url(r'^html/', ProjectView.as_view()),
    url(r'^page1/', Page1View.as_view()),
    url(r'^page2/', Page2View.as_view()),
    url(r'^page3/', Page3View.as_view()),
]

